require 'test_helper'

class CoffeeHouseTest < ActiveSupport::TestCase
fixtures :coffee_houses  

  test "should save post with name" do
		coffee_house = coffee_houses(:one)
		assert coffee_house.save, "Saved the coffee house with a name"
 end
  test "should not save post without name" do
		coffee_house = coffee_houses(:two)
		assert coffee_house.save, "Saved the coffee house without a name"
	end
end
