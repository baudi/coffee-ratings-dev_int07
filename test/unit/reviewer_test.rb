require 'test_helper'

class ReviewerTest < ActiveSupport::TestCase

  fixtures :reviewers  
 
  test "should save post with name" do
		reviewer = reviewers(:one)
		assert reviewer.save, "Saved the Reviewer with name"
  end
  
  test "should not save post without name" do
		reviewer = reviewers(:two)
		assert reviewer.save, "Saved the Reviewer without name"
  end

  test "should save post with correct email format" do
		reviewer = reviewers(:three)
		assert reviewer.save, "Correct email format"
  end

  test "should not save post without correct email format" do
		reviewer = reviewers(:four)
		assert reviewer.save, "Error email format"
  end
end
