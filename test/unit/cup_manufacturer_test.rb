require 'test_helper'

class CupManufacturerTest < ActiveSupport::TestCase
	fixtures :cup_manufacturers

	test "should not save cup manufacturer without name" do
		cm = cup_manufacturers(:one)
		assert cm.save, "saved the cup manufacturer without a name"
	end

	test "should not save cup manufacturer a url with sintax error" do
	  cm = cup_manufacturers(:two)
		assert cm.save ,"saved the cup manufacturer with wrong url "
	end
end
