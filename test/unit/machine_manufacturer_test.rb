require 'test_helper'

class MachineManufacturerTest < ActiveSupport::TestCase

  fixtures :machine_manufacturers  
 
  test "should save post with name" do
		machine_manufacturer = machine_manufacturers(:one)
		assert machine_manufacturer.save, "Saved the Machine Manufacturer with name"
  end

  test "should not save post without name" do
		machine_manufacturer = machine_manufacturers(:two)
		assert machine_manufacturer.save, "Saved the Machine Manufacturer without name"
  end

  test "should save Url" do
		machine_manufacturer = machine_manufacturers(:three)
		assert machine_manufacturer.save, "Correct"
  end

  test "should not save Url" do
		machine_manufacturer = machine_manufacturers(:four)
		assert machine_manufacturer.save, "Url error"
  end
end
