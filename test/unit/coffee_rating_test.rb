require 'test_helper'

class CoffeeRatingTest < ActiveSupport::TestCase
	fixtures :coffee_ratings  

  test "should save post with rates" do
		coffee_rating = coffee_ratings(:one)
		assert coffee_rating.save, "Saved the coffee rating with rates"
  end
  test "should not save post without rates" do
		coffee_rating = coffee_ratings(:two)
		assert coffee_rating.save, "Saved the coffee rating without rates"
  end
end
