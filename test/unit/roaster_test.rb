require 'test_helper'

class RoasterTest < ActiveSupport::TestCase
	fixtures :roasters

	test "should not save roaster without name" do
		roaster = roasters(:one)
		assert roaster.save, "saved the roaster without a name"
	end

	test "should not save roaster a url with sintax error" do
	  roaster = roasters(:three)
		assert roaster.save ,"saved the roaster with wrong url "
	end
end
