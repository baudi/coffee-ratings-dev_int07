require 'test_helper'

class CupManufacturersControllerTest < ActionController::TestCase
  setup do
    @cup_manufacturer = cup_manufacturers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cup_manufacturers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cup_manufacturer" do
    assert_difference('CupManufacturer.count') do
      post :create, cup_manufacturer: { comments: @cup_manufacturer.comments, description: @cup_manufacturer.description, name: @cup_manufacturer.name, website: @cup_manufacturer.website }
    end

    assert_redirected_to cup_manufacturer_path(assigns(:cup_manufacturer))
  end

  test "should show cup_manufacturer" do
    get :show, id: @cup_manufacturer
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cup_manufacturer
    assert_response :success
  end

  test "should update cup_manufacturer" do
    put :update, id: @cup_manufacturer, cup_manufacturer: { comments: @cup_manufacturer.comments, description: @cup_manufacturer.description, name: @cup_manufacturer.name, website: @cup_manufacturer.website }
    assert_redirected_to cup_manufacturer_path(assigns(:cup_manufacturer))
  end

  test "should destroy cup_manufacturer" do
    assert_difference('CupManufacturer.count', -1) do
      delete :destroy, id: @cup_manufacturer
    end

    assert_redirected_to cup_manufacturers_path
  end
end
