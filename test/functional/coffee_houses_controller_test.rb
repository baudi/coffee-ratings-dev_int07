require 'test_helper'

class CoffeeHousesControllerTest < ActionController::TestCase
  setup do
    @coffee_house = coffee_houses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:coffee_houses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create coffee_house" do
    assert_difference('CoffeeHouse.count') do
      post :create, coffee_house: { comments: @coffee_house.comments, hours: @coffee_house.hours, name: @coffee_house.name, type_of_establishment: @coffee_house.type_of_establishment, website: @coffee_house.website }
    end

    assert_redirected_to coffee_house_path(assigns(:coffee_house))
  end

  test "should show coffee_house" do
    get :show, id: @coffee_house
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @coffee_house
    assert_response :success
  end

  test "should update coffee_house" do
    put :update, id: @coffee_house, coffee_house: { comments: @coffee_house.comments, hours: @coffee_house.hours, name: @coffee_house.name, type_of_establishment: @coffee_house.type_of_establishment, website: @coffee_house.website }
    assert_redirected_to coffee_house_path(assigns(:coffee_house))
  end

  test "should destroy coffee_house" do
    assert_difference('CoffeeHouse.count', -1) do
      delete :destroy, id: @coffee_house
    end

    assert_redirected_to coffee_houses_path
  end
end
