require 'test_helper'

class MachineManufacturersControllerTest < ActionController::TestCase
  setup do
    @machine_manufacturer = machine_manufacturers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:machine_manufacturers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create machine_manufacturer" do
    assert_difference('MachineManufacturer.count') do
      post :create, machine_manufacturer: { comments: @machine_manufacturer.comments, groups: @machine_manufacturer.groups, models: @machine_manufacturer.models, name: @machine_manufacturer.name, types: @machine_manufacturer.types, website: @machine_manufacturer.website }
    end

    assert_redirected_to machine_manufacturer_path(assigns(:machine_manufacturer))
  end

  test "should show machine_manufacturer" do
    get :show, id: @machine_manufacturer
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @machine_manufacturer
    assert_response :success
  end

  test "should update machine_manufacturer" do
    put :update, id: @machine_manufacturer, machine_manufacturer: { comments: @machine_manufacturer.comments, groups: @machine_manufacturer.groups, models: @machine_manufacturer.models, name: @machine_manufacturer.name, types: @machine_manufacturer.types, website: @machine_manufacturer.website }
    assert_redirected_to machine_manufacturer_path(assigns(:machine_manufacturer))
  end

  test "should destroy machine_manufacturer" do
    assert_difference('MachineManufacturer.count', -1) do
      delete :destroy, id: @machine_manufacturer
    end

    assert_redirected_to machine_manufacturers_path
  end
end
