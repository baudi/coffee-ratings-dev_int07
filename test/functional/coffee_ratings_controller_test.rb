require 'test_helper'

class CoffeeRatingsControllerTest < ActionController::TestCase
  setup do
    @coffee_rating = coffee_ratings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:coffee_ratings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create coffee_rating" do
    assert_difference('CoffeeRating.count') do
      post :create, coffee_rating: { ambiance_rate: @coffee_rating.ambiance_rate, aroma_rate: @coffee_rating.aroma_rate, barista_rate: @coffee_rating.barista_rate, body_rate: @coffee_rating.body_rate, brightness_rate: @coffee_rating.brightness_rate, cafe_r: @coffee_rating.cafe_r, coffee_r: @coffee_rating.coffee_r, consistency: @coffee_rating.consistency, crema_rate: @coffee_rating.crema_rate, flavor_rate: @coffee_rating.flavor_rate, presentation_rate: @coffee_rating.presentation_rate, price: @coffee_rating.price, primary_taste: @coffee_rating.primary_taste, savvy_rate: @coffee_rating.savvy_rate, size: @coffee_rating.size, taster_correction: @coffee_rating.taster_correction }
    end

    assert_redirected_to coffee_rating_path(assigns(:coffee_rating))
  end

  test "should show coffee_rating" do
    get :show, id: @coffee_rating
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @coffee_rating
    assert_response :success
  end

  test "should update coffee_rating" do
    put :update, id: @coffee_rating, coffee_rating: { ambiance_rate: @coffee_rating.ambiance_rate, aroma_rate: @coffee_rating.aroma_rate, barista_rate: @coffee_rating.barista_rate, body_rate: @coffee_rating.body_rate, brightness_rate: @coffee_rating.brightness_rate, cafe_r: @coffee_rating.cafe_r, coffee_r: @coffee_rating.coffee_r, consistency: @coffee_rating.consistency, crema_rate: @coffee_rating.crema_rate, flavor_rate: @coffee_rating.flavor_rate, presentation_rate: @coffee_rating.presentation_rate, price: @coffee_rating.price, primary_taste: @coffee_rating.primary_taste, savvy_rate: @coffee_rating.savvy_rate, size: @coffee_rating.size, taster_correction: @coffee_rating.taster_correction }
    assert_redirected_to coffee_rating_path(assigns(:coffee_rating))
  end

  test "should destroy coffee_rating" do
    assert_difference('CoffeeRating.count', -1) do
      delete :destroy, id: @coffee_rating
    end

    assert_redirected_to coffee_ratings_path
  end
end
