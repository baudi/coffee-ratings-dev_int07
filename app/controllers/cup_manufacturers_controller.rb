class CupManufacturersController < ApplicationController
  # GET /cup_manufacturers
  # GET /cup_manufacturers.json
  def index
    @cup_manufacturers = CupManufacturer.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @cup_manufacturers }
    end
  end

  # GET /cup_manufacturers/1
  # GET /cup_manufacturers/1.json
  def show
    @cup_manufacturer = CupManufacturer.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @cup_manufacturer }
    end
  end

  # GET /cup_manufacturers/new
  # GET /cup_manufacturers/new.json
  def new
    @cup_manufacturer = CupManufacturer.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @cup_manufacturer }
    end
  end

  # GET /cup_manufacturers/1/edit
  def edit
    @cup_manufacturer = CupManufacturer.find(params[:id])
  end

  # POST /cup_manufacturers
  # POST /cup_manufacturers.json
  def create
    @cup_manufacturer = CupManufacturer.new(params[:cup_manufacturer])

    respond_to do |format|
      if @cup_manufacturer.save
        format.html { redirect_to @cup_manufacturer, notice: 'Cup manufacturer was successfully created.' }
        format.json { render json: @cup_manufacturer, status: :created, location: @cup_manufacturer }
      else
        format.html { render action: "new" }
        format.json { render json: @cup_manufacturer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /cup_manufacturers/1
  # PUT /cup_manufacturers/1.json
  def update
    @cup_manufacturer = CupManufacturer.find(params[:id])

    respond_to do |format|
      if @cup_manufacturer.update_attributes(params[:cup_manufacturer])
        format.html { redirect_to @cup_manufacturer, notice: 'Cup manufacturer was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @cup_manufacturer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cup_manufacturers/1
  # DELETE /cup_manufacturers/1.json
  def destroy
    @cup_manufacturer = CupManufacturer.find(params[:id])
    @cup_manufacturer.destroy

    respond_to do |format|
      format.html { redirect_to cup_manufacturers_url }
      format.json { head :no_content }
    end
  end
end
