class CoffeeRatingsController < ApplicationController
  # GET /coffee_ratings
  # GET /coffee_ratings.json
  def index
    @coffee_ratings = CoffeeRating.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @coffee_ratings.to_json(:include => [:coffee_house]) }
    end
  end

  # GET /coffee_ratings/1
  # GET /coffee_ratings/1.json
  def show
    @coffee_rating = CoffeeRating.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @coffee_rating }
    end
  end

  # GET /coffee_ratings/new
  # GET /coffee_ratings/new.json
  def new
    @coffee_rating = CoffeeRating.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @coffee_rating }
    end
  end

  # GET /coffee_ratings/1/edit
  def edit
    @coffee_rating = CoffeeRating.find(params[:id])
  end

  # POST /coffee_ratings
  # POST /coffee_ratings.json
  def create
    @coffee_rating = CoffeeRating.new(params[:coffee_rating])

    respond_to do |format|
      if @coffee_rating.save
        format.html { redirect_to @coffee_rating, notice: 'Coffee rating was successfully created.' }
        format.json { render json: @coffee_rating, status: :created, location: @coffee_rating }
      else
        format.html { render action: "new" }
        format.json { render json: @coffee_rating.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /coffee_ratings/1
  # PUT /coffee_ratings/1.json
  def update
    @coffee_rating = CoffeeRating.find(params[:id])

    respond_to do |format|
      if @coffee_rating.update_attributes(params[:coffee_rating])
        format.html { redirect_to @coffee_rating, notice: 'Coffee rating was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @coffee_rating.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /coffee_ratings/1
  # DELETE /coffee_ratings/1.json
  def destroy
    @coffee_rating = CoffeeRating.find(params[:id])
    @coffee_rating.destroy

    respond_to do |format|
      format.html { redirect_to coffee_ratings_url }
      format.json { head :no_content }
    end
  end

  # Recovers CoffeeRatings
  def recover
	@coffee_ratings = CoffeeRating.find(:all, :offset=> params[:start], :limit => params[:limit])
	@count = CoffeeRating.count

	respond_to do |format|
      format.html # index.html.erb
      format.json { render json: {:count => @count, :ratings => JSON.parse(@coffee_ratings.to_json(:include => [:coffee_house]))}}
	end
  end

  # Calculates rate averages
  def rate_averages
	@pm_aroma = CoffeeRating.average("aroma_rate")
	@pm_body = CoffeeRating.average("body_rate")
	@pm_brightness = CoffeeRating.average("brightness_rate")
	@pm_barista = CoffeeRating.average("barista_rate")
	@pm_savvy = CoffeeRating.average("savvy_rate")
	@pm_ambiance = CoffeeRating.average("ambiance_rate")
	@pm_presentation = CoffeeRating.average("presentation_rate")
	@pm_crema = CoffeeRating.average("crema_rate")
	@pm_flavor = CoffeeRating.average("flavor_rate")

	@responseToSend = {:aroma => @pm_aroma, :body => @pm_body, :brightness => @pm_brightness, :barista => @pm_barista, :savvy => @pm_savvy, :ambiance => @pm_ambiance, :presentation => @pm_presentation, :crema => @pm_crema, :flavor => @pm_flavor}
	respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @responseToSend}
	end
  end
end
