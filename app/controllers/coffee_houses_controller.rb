class CoffeeHousesController < ApplicationController
  # GET /coffee_houses
  # GET /coffee_houses.json
  def index
    @coffee_houses = CoffeeHouse.all

    respond_to do |format|
      format.html # index.html.erb
	  format.json {render json: @coffee_houses.to_json(:include => [:roaster, :cup_manufacturer, :machine_manufacturer, :reviewer])}
    end
  end

  # GET /coffee_houses/1
  # GET /coffee_houses/1.json
  def show
    @coffee_house = CoffeeHouse.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @coffee_house }
    end
  end

  # GET /coffee_houses/new
  # GET /coffee_houses/new.json
  def new
    @coffee_house = CoffeeHouse.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @coffee_house }
    end
  end

  # GET /coffee_houses/1/edit
  def edit
    @coffee_house = CoffeeHouse.find(params[:id])
  end

  # POST /coffee_houses
  # POST /coffee_houses.json
  def create
    @coffee_house = CoffeeHouse.new(params[:coffee_house])

    respond_to do |format|
      if @coffee_house.save
        format.html { redirect_to @coffee_house, notice: 'Coffee house was successfully created.' }
        format.json { render json: @coffee_house, status: :created, location: @coffee_house }
      else
        format.html { render action: "new" }
        format.json { render json: @coffee_house.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /coffee_houses/1
  # PUT /coffee_houses/1.json
  def update
    @coffee_house = CoffeeHouse.find(params[:id])

    respond_to do |format|
      if @coffee_house.update_attributes(params[:coffee_house])
        format.html { redirect_to @coffee_house, notice: 'Coffee house was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @coffee_house.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /coffee_houses/1
  # DELETE /coffee_houses/1.json
  def destroy
    @coffee_house = CoffeeHouse.find(params[:id])
    @coffee_house.destroy

    respond_to do |format|
      format.html { redirect_to coffee_houses_url }
      format.json { head :no_content }
    end
  end
  
  # Calculates total of Coffee Houses for each Roaster, Cup Manufacturer and Machine Manufacturer
  def average
    @roaster = Roaster.includes(:coffee_houses, :address).where(:coffee_houses => {:id => params[:id]}).first

	@cm = CupManufacturer.includes(:coffee_houses, :address).where(:coffee_houses => {:id => params[:id]}).first

	@mm = MachineManufacturer.includes(:coffee_houses, :address).where(:coffee_houses => {:id => params[:id]}).first

	@cont_r = CoffeeHouse.joins(:roaster, :coffee_ratings).where(:roasters => {:id => @roaster.id}).count

	@cont_cm = CoffeeHouse.joins(:cup_manufacturer, :coffee_ratings).where(:cup_manufacturers => {:id => @cm.id}).count

	@cont_mm = CoffeeHouse.joins(:machine_manufacturer, :coffee_ratings).where(:machine_manufacturers => {:id => @mm.id}).count

	respond_to do |format|
	  format.html
	  format.json { render :json => {:count_r => @cont_r, :r_name => @roaster.name, :r_country => @roaster.address.country, :r_phone => @roaster.address.phone_number, :url => @roaster.website, :count_cm => @cont_cm, :cm_name => @cm.name, :cm_country => @cm.address.country, :cm_phone => @cm.address.phone_number, :cm_url => @cm.website, :count_mm => @cont_mm, :mm_name => @mm.name, :mm_country => @mm.address.country, :mm_phone => @mm.address.phone_number, :mm_url => @mm.website}}
	end
  end
	
  # Calculates Rating's averages for Roaster, Cup Manufacturer and Machine Manufacturer
  def average_customized
	@coffee_house = CoffeeHouse.find(params[:id])
 	@rat = @coffee_house.coffee_ratings.first

	@roaster = Roaster.includes(:coffee_houses).where(:coffee_houses => {:id => params[:id]}).first

	@cm = CupManufacturer.includes(:coffee_houses, :address).where(:coffee_houses => {:id => params[:id]}).first

	@mm = MachineManufacturer.includes(:coffee_houses, :address).where(:coffee_houses => {:id => params[:id]}).first

	pm_r = CoffeeRating.joins(:coffee_house).average(:coffee_r, :conditions => ['roaster_id = ?',@roaster.id])
	pm_cm = CoffeeRating.joins(:coffee_house).average(:coffee_r, :conditions => ['cup_manufacturer_id = ?',@cm.id])
	pm_mm = CoffeeRating.joins(:coffee_house).average(:coffee_r, :conditions => ['machine_manufacturer_id = ?',@mm.id])

	element_r = [{:name => 'aroma', :rating => @rat.aroma_rate, :pm => pm_r},{:name => 'body', :rating => @rat.body_rate, :pm => pm_r},{:name => 'brightness', :rating => @rat.brightness_rate, :pm => pm_r},{:name => 'crema', :rating => @rat.crema_rate, :pm => pm_r},{:name => 'flavor', :rating => @rat.flavor_rate, :pm => pm_r}]

	element_cm = [{:name => 'aroma', :rating => @rat.aroma_rate, :pm => pm_cm},{:name => 'body', :rating => @rat.body_rate, :pm => pm_cm},{:name => 'brightness', :rating => @rat.brightness_rate, :pm => pm_cm},{:name => 'crema', :rating => @rat.crema_rate, :pm => pm_cm},{:name => 'flavor', :rating => @rat.flavor_rate, :pm => pm_cm}]

	element_mm = [{:name => 'aroma', :rating => @rat.aroma_rate, :pm => pm_mm},{:name => 'body', :rating => @rat.body_rate, :pm => pm_mm},{:name => 'brightness', :rating => @rat.brightness_rate, :pm => pm_mm},{:name => 'crema', :rating => @rat.crema_rate, :pm => pm_mm},{:name => 'flavor', :rating => @rat.flavor_rate, :pm => pm_mm}]

	arrayToSend = {:average_rating_per_roaster => element_r, :average_rating_per_cup_manufacturer => element_cm, :average_rating_per_machine_manufacturer => element_mm}

	respond_to do |format|
	  format.html
      format.json { render :json => arrayToSend}
	end
  end
end
