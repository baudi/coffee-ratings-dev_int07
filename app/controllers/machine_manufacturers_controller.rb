class MachineManufacturersController < ApplicationController
  # GET /machine_manufacturers
  # GET /machine_manufacturers.json
  def index
    @machine_manufacturers = MachineManufacturer.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @machine_manufacturers }
    end
  end

  # GET /machine_manufacturers/1
  # GET /machine_manufacturers/1.json
  def show
    @machine_manufacturer = MachineManufacturer.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @machine_manufacturer }
    end
  end

  # GET /machine_manufacturers/new
  # GET /machine_manufacturers/new.json
  def new
    @machine_manufacturer = MachineManufacturer.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @machine_manufacturer }
    end
  end

  # GET /machine_manufacturers/1/edit
  def edit
    @machine_manufacturer = MachineManufacturer.find(params[:id])
  end

  # POST /machine_manufacturers
  # POST /machine_manufacturers.json
  def create
    @machine_manufacturer = MachineManufacturer.new(params[:machine_manufacturer])

    respond_to do |format|
      if @machine_manufacturer.save
        format.html { redirect_to @machine_manufacturer, notice: 'Machine manufacturer was successfully created.' }
        format.json { render json: @machine_manufacturer, status: :created, location: @machine_manufacturer }
      else
        format.html { render action: "new" }
        format.json { render json: @machine_manufacturer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /machine_manufacturers/1
  # PUT /machine_manufacturers/1.json
  def update
    @machine_manufacturer = MachineManufacturer.find(params[:id])

    respond_to do |format|
      if @machine_manufacturer.update_attributes(params[:machine_manufacturer])
        format.html { redirect_to @machine_manufacturer, notice: 'Machine manufacturer was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @machine_manufacturer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /machine_manufacturers/1
  # DELETE /machine_manufacturers/1.json
  def destroy
    @machine_manufacturer = MachineManufacturer.find(params[:id])
    @machine_manufacturer.destroy

    respond_to do |format|
      format.html { redirect_to machine_manufacturers_url }
      format.json { head :no_content }
    end
  end
end
