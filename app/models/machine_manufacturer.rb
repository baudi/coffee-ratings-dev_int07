class MachineManufacturer < ActiveRecord::Base
  has_many :coffee_houses
  has_one :address, :as => :addressable
    attr_accessible :comments, :groups, :models, :name, :types, :website, :created_at, :updated_at, :address
  validates :name, :presence => true
end
