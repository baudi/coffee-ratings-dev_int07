class CupManufacturer < ActiveRecord::Base
  has_many :coffee_houses
  has_one :address, :as => :addressable
  validates :name, :presence => true
attr_accessible :comments, :description, :name, :website, :created_at, :updated_at, :address

end
