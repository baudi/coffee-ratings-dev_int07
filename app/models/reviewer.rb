class Reviewer < ActiveRecord::Base
  has_many :coffee_houses
  has_one :coffee_rating   
  validates :name, :presence => true 
  validates :email,	:format => {:with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i}
     
  attr_accessible :email, :name, :nickname, :reviewer_type
end
