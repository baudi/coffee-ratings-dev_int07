class CoffeeHouse < ActiveRecord::Base
  belongs_to :roaster
  belongs_to :cup_manufacturer
  belongs_to :machine_manufacturer
  belongs_to :reviewer
  has_one :address, :as => :addressable
  has_many :coffee_ratings
  validates :name, :presence => true
 attr_accessible :comments, :hours, :name, :type_of_establishment, :website, :created_at, :updated_at, :address, :roaster, :machine_manufacturer, :cup_manufacturer, :reviewer
end
