class CoffeeRating < ActiveRecord::Base
  belongs_to :coffee_house
  belongs_to :reviewer
  validates :ambiance_rate, :aroma_rate, :barista_rate, :body_rate, :brightness_rate,:crema_rate, :flavor_rate, :presentation_rate, :savvy_rate, :taster_correction, :presence => true
attr_accessible :ambiance_rate, :aroma_rate, :coffee_r, :cafe_r, :barista_rate, :body_rate, :brightness_rate, :consistency, :crema_rate, :flavor_rate, :presentation_rate, :price, :primary_taste, :savvy_rate, :size, :taster_correction, :created_at, :updated_at, :coffee_house, :reviewer
end
