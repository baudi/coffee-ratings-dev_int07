class Address < ActiveRecord::Base
attr_accessible :addressable_id, :addressable_type, :city, :country, :neighborhood, :phone_number, :state, :street_line_1, :street_line_2, :zip_code
belongs_to :addressable, :polymorphic => true
end
