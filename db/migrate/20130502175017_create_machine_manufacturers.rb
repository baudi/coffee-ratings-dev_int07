class CreateMachineManufacturers < ActiveRecord::Migration
  def change
    create_table :machine_manufacturers do |t|
      t.string :name
      t.string :types
      t.string :groups
      t.string :models
      t.string :website
      t.text :comments

      t.timestamps
    end
  end
end
