class CreateCoffeeHouses < ActiveRecord::Migration
  def change
    create_table :coffee_houses do |t|
      t.references :roaster
      t.references :cup_manufacturer
      t.references :machine_manufacturer
      t.references :reviewer
      t.string :name
      t.string :hours
      t.string :type_of_establishment
      t.string :website
      t.text :comments

      t.timestamps
    end
    add_index :coffee_houses, :roaster_id
    add_index :coffee_houses, :cup_manufacturer_id
    add_index :coffee_houses, :machine_manufacturer_id
    add_index :coffee_houses, :reviewer_id
  end
end
