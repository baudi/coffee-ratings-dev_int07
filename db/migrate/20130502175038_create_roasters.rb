class CreateRoasters < ActiveRecord::Migration
  def change
    create_table :roasters do |t|
      t.string :name
      t.string :blends
      t.string :website
      t.text :comments

      t.timestamps
    end
  end
end
