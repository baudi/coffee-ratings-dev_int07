class CreateCoffeeRatings < ActiveRecord::Migration
  def change
    create_table :coffee_ratings do |t|
      t.references :coffee_house
      t.references :reviewer
      t.integer :barista_rate
      t.integer :savvy_rate
      t.integer :ambiance_rate
      t.integer :presentation_rate
      t.integer :aroma_rate
      t.integer :body_rate
      t.integer :brightness_rate
      t.integer :crema_rate
      t.integer :flavor_rate
      t.float :taster_correction
      t.string :consistency
      t.string :primary_taste
      t.string :size
      t.float :price
      t.float :cafe_r
      t.float :coffee_r

      t.timestamps
    end
    add_index :coffee_ratings, :coffee_house_id
    add_index :coffee_ratings, :reviewer_id
  end
end
