class CreateReviewers < ActiveRecord::Migration
  def change
    create_table :reviewers do |t|
      t.string :name
      t.string :nickname
      t.string :email
      t.string :reviewer_type

      t.timestamps
    end
  end
end
