class CreateCupManufacturers < ActiveRecord::Migration
  def change
    create_table :cup_manufacturers do |t|
      t.string :name
      t.text :description
      t.string :website
      t.text :comments

      t.timestamps
    end
  end
end
