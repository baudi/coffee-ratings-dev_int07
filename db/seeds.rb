require 'rubygems'
require 'nokogiri'
require 'open-uri'
require 'date'

# Creates a Reviewer
@reviewer = Reviewer.find_or_create_by_name({:email => "reviewer@site.com", :name => "Greg Sherwin", :nickname => "Greg", :reviewer_type => "Staff"})

site_url = "http://coffeeratings.com"
page = Nokogiri::HTML(open(site_url))

# Lists all page's links
list_all_page = page.css("div#resultNav a")
list_all_page.each do |page|
  page_url = site_url+page["href"]
  puts  page_url
  open_page = Nokogiri::HTML(open(page_url))
  links_of_pages = open_page.css('table.results tr td a')

  # Lists all Coffee House's pages
  links_of_pages.each do |item|
    coffee_house_link = site_url + item["href"]
   	puts "\t Page of CoffeeHouse: #{coffee_house_link}"
	page_ch = Nokogiri::HTML(open(coffee_house_link))
	if page_ch.at_css('tr#cafeName td:nth-child(1)')
	  name_ch = page_ch.css('tr#cafeName td:nth-child(1)').text
	  recover = page_ch.css('table.results tr td:nth-child(2n)')
	  street_line_1_ch = recover[0].text
	  street_line_2_ch = recover[1].text
	  # Regular Expression for City, State Zip
	  reg_expr = /([\w\s]*)?,?\s?+([A-Z]{2,3})?\s?+(\d{1,5})?/
	  match_text = reg_expr.match(recover[2].text)
	  unless match_text
	    city_ch = nil
	    state_ch = nil
	    zip_code_ch = nil
	  end
	  city_ch = match_text[1]
	  state_ch = match_text[2]
	  zip_code_ch = match_text[3]
	  neighborhood_ch = recover[3].text
	  phone_number_ch = recover[4].text
	  hours_ch = recover[5].text
	  type_of_establishment_ch = recover[6].text
	  barista_rate_cr = recover[8].text.to_f
	  savvy_rate_cr = recover[9].text.to_f
	  ambiance_rate_cr = recover[10].text.to_f
	  presentation_rate_cr = recover[11].text.to_f
	  consistency_cr = recover[12].text
	  aroma_cr = recover[15].text.to_f
	  body_rate_cr = recover[16].text.to_f
	  brightness_rate_cr = recover[17].text.to_f
	  crema_rate_cr = recover[18].text.to_f
	  flavor_rate_cr = recover[19].text.to_f
	  taster_correction_cr = recover[20].text.to_f
	  size_cr = recover[21].text
	  primary_taste_cr = recover[22].text
	  price_cr = recover[25].text.byteslice(1,recover[25].text.length)
	  l_date = recover[26].text.split("/")
	  l_update_ch = Date.parse(l_date[2]+"-" + l_date[0]+"-" + l_date[1])
	  f_date = recover[27].text.split("/")
	  f_reviewed_ch = Date.parse(f_date[2] + "-" + f_date[0] + "-" + f_date[1])
	  comments_ch = page_ch.css('td#reviewBody').text
	  coffee_r = (aroma_cr + body_rate_cr + brightness_rate_cr + crema_rate_cr + flavor_rate_cr + taster_correction_cr)/5.0
	  cafe_r = (barista_rate_cr + savvy_rate_cr + ambiance_rate_cr + presentation_rate_cr)/ 4.0

      page_coffee_house = Nokogiri::HTML(open(coffee_house_link))
      links_level2 = page_coffee_house.css("table.results tr td a")
      links_level2.each do |link|
        if link['href'].include? 'roasterId'
          link_roaster = site_url + link['href']
	      # Prints the Roaster's link
          puts "\t \t ROASTER: #{link_roaster}"
          # Recovers Roaster
	      page_roaster = Nokogiri::HTML(open(link_roaster))
	      name_ro = page_roaster.css('tr#cafeName td:nth-child(1)').text
	      recover = page_roaster.css('table.results tr td:nth-child(2n)')
	      street_line_1_r= recover[0].text
	      # Regular Expres for City, State Zip
	      reg_expr = /([\w\s]*)?,?\s?+([A-Z]{2,3})?\s?+(\d{1,5})?/
	      match_text = reg_expr.match(recover[1].text)
	      unless match_text
	 	    city_r = nil
		    state_r = nil
		    zip_code_r = nil
	      end
	      city_r = match_text[1]
	      state_r = match_text[2]
	      zip_code_r = match_text[3]
	      country_r = recover[2].text
	      phone_number_r = recover[3].text
	      website_r = recover[4].text
	      blends = recover[5].text
	      l_date = recover[6].text.split("/")
	      l_update = Date.parse(l_date[2] + "-" + l_date[0] + "-" + l_date[1])
	      f_date = recover[7].text.split("/")
	      f_entered = Date.parse(f_date[2] + "-" + f_date[0] + "-" + f_date[1])
	      comments_ro = page_roaster.css('td#reviewBody').text

	      address_r = Address.new({:city => city_r,:country => country_r, :phone_number => phone_number_r, :state => state_r, :street_line_1 => street_line_1_r, :street_line_2 => nil, :zip_code => zip_code_r})
	      @roaster = Roaster.find_or_initialize_by_name({:blends => blends, :comments => comments_ro, :name => name_ro, :website => website_r, :created_at => f_entered, :updated_at => l_update})
	      if !@roaster.persisted?
		    @roaster.address=address_r
		    @roaster.save
	      end
	      # End Roaster
        end
        if link['href'].include? 'cup-view'
          link_cup_manufacturer = site_url+link['href']
          puts "\t \t CUP MANUFACTURER: #{link_cup_manufacturer}"
          # Recovers Cup Manufacturer
      	  page_cm = Nokogiri::HTML(open(link_cup_manufacturer))
      	  name_cm = page_cm.css('tr#cafeName td:nth-child(1)').text
      	  recover = page_cm.css('table.display tr>td.split table.results tr td:nth-child(2n)')
      	  street_line_1_cm = recover[0].text
      	  # Regular Expres for City, State Zip
      	  reg_expr = /([\w\s]*)?,?\s?+([A-Z]{2,3})?\s?+(\d{1,5})?/
      	  match_text = reg_expr.match(recover[1].text)
      	  unless match_text
	     	city_cm = nil
	        state_cm = nil
	        zip_code_cm = nil
          end
          city_cm = match_text[1]
      	  state_cm = match_text[2]
      	  zip_code_cm = match_text[3]
      	  country_cm = recover[2].text
      	  phone_number_cm = recover[3].text
      	  website_cm = recover[4].text
      	  description_cm = recover[5].text
      	  l_date = recover[6].text.split("/")
      	  l_update_cm = Date.parse(l_date[2] + "-" + l_date[0] + "-" + l_date[1])
      	  f_date = recover[7].text.split("/")
      	  f_entered_cm = Date.parse(f_date[2] + "-" + f_date[0] + "-" + f_date[1])
      	  comments_cm = page_cm.css('td#reviewBody').text
  
      	  # Finds or creates Address by phone number
          address_cm = Address.new({:city => city_cm,:country => country_cm, :phone_number => phone_number_cm, :state => state_cm, :street_line_1 => street_line_1_cm, :street_line_2 => nil, :zip_code => zip_code_cm})
          @cup_manufacturer = CupManufacturer.find_or_initialize_by_name({:comments => comments_cm, :description => description_cm, :name => name_cm, :website => website_cm, :created_at => f_entered_cm, :updated_at => l_update_cm})

          if !@cup_manufacturer.persisted?
	        @cup_manufacturer.address=address_cm
	        @cup_manufacturer.save
          end
          # End Cup Manufacturer
        end
        if link['href'].include? 'machine-view'
          link_machine_manufacturer = site_url+link['href']
          puts "\t \t MACHINE MANUFACTURER: #{link_machine_manufacturer}"
          # Recovers Machine Manufacturer
          page_mm = Nokogiri::HTML(open(link_machine_manufacturer))
     	  name_mm = page_mm.css('tr#cafeName td:nth-child(1)').text
    	  recover = page_mm.css('table.display tr>td.split table.results tr td:nth-child(2n)');
    	  street_line_1_mm = recover[0].text
    	  # Regular Expres for City, State Zip
    	  reg_expr = /([\w\s]*)?,?\s?+([A-Z]{2,3})?\s?+(\d{1,5})?/
    	  match_text = reg_expr.match(recover[1].text)
    	  unless match_text
      	    city_mm = nil
      		state_mm = nil
      		zip_code_mm = nil
    	  end
    	  city_mm = match_text[1]
    	  state_mm = match_text[2]
    	  zip_code_mm = match_text[3]
    	  country_mm = recover[2].text
    	  phone_number_mm = recover[3].text
    	  website_mm = recover[4].text
    	  types_mm = recover[5].text
    	  groups_mm = recover[6].text
    	  models_mm = recover[7].text
    	  l_date = recover[8].text.split("/")
    	  l_update_mm = Date.parse(l_date[2] + "-" + l_date[0] + "-" + l_date[1])
    	  f_date = recover[9].text.split("/")
    	  f_entered_mm = Date.parse(f_date[2] + "-" + f_date[0] + "-" + f_date[1])
    	  comments_mm = page_mm.css('td#reviewBody').text
    	  address_mm = Address.new({:city => city_mm,:country => country_mm, :phone_number =>   phone_number_mm, :state => state_mm, :street_line_1 => street_line_1_mm, :street_line_2 => nil, :zip_code => zip_code_mm})
    	  @machine_manufacturer = MachineManufacturer.find_or_initialize_by_name({:comments =>   comments_mm, :groups => groups_mm, :models => models_mm, :name => name_mm, :types => types_mm, :website => website_mm, :created_at => f_entered_mm, :updated_at => l_update_mm})
    	  if !@machine_manufacturer.persisted?
      		@machine_manufacturer.address=address_mm
      		@machine_manufacturer.save
    	  end
    	  # End Machine Manufacturer
  		end
	  end
	  #Recovers Coffee House
  	  address_ch = Address.find_or_initialize_by_street_line_1_and_phone_number({:city => city_ch,:country => nil, :neighborhood => nil, :phone_number => phone_number_ch, :state => state_ch, :street_line_1 => street_line_1_ch, :street_line_2 => street_line_2_ch, :zip_code => zip_code_ch})

	  if !address_ch.persisted?
    	coffee_house = CoffeeHouse.create({:comments => comments_ch, :hours => hours_ch, :name => 		name_ch, :type_of_establishment => type_of_establishment_ch, :website => nil, :created_at => f_reviewed_ch, :updated_at => l_update_ch, :address => address_ch, :roaster => @roaster, :machine_manufacturer => @machine_manufacturer, :cup_manufacturer => @cup_manufacturer, :reviewer => @reviewer})

  	  	coffee_rating = CoffeeRating.create({:ambiance_rate => ambiance_rate_cr, :aroma_rate => aroma_cr, :barista_rate => barista_rate_cr, :body_rate => body_rate_cr, :brightness_rate => brightness_rate_cr, :consistency => consistency_cr, :crema_rate => crema_rate_cr, :flavor_rate => flavor_rate_cr, :presentation_rate => presentation_rate_cr, :price => price_cr, :primary_taste => primary_taste_cr, :savvy_rate => savvy_rate_cr, :size => size_cr, :taster_correction => taster_correction_cr, :coffee_r => coffee_r, :cafe_r => cafe_r, :created_at => f_reviewed_ch, :updated_at => l_update_ch, :coffee_house => coffee_house, :reviewer => @reviewer})
	  end
      #End Coffee House
	end
  end #End link coffee House
end #End link pages
