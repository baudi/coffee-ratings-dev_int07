# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130502193448) do

  create_table "addresses", :force => true do |t|
    t.integer  "addressable_id"
    t.string   "addressable_type"
    t.string   "street_line_1"
    t.string   "street_line_2"
    t.string   "neighborhood"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.string   "zip_code"
    t.string   "phone_number"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "coffee_houses", :force => true do |t|
    t.integer  "roaster_id"
    t.integer  "cup_manufacturer_id"
    t.integer  "machine_manufacturer_id"
    t.integer  "reviewer_id"
    t.string   "name"
    t.string   "hours"
    t.string   "type_of_establishment"
    t.string   "website"
    t.text     "comments"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "coffee_houses", ["cup_manufacturer_id"], :name => "index_coffee_houses_on_cup_manufacturer_id"
  add_index "coffee_houses", ["machine_manufacturer_id"], :name => "index_coffee_houses_on_machine_manufacturer_id"
  add_index "coffee_houses", ["reviewer_id"], :name => "index_coffee_houses_on_reviewer_id"
  add_index "coffee_houses", ["roaster_id"], :name => "index_coffee_houses_on_roaster_id"

  create_table "coffee_ratings", :force => true do |t|
    t.integer  "coffee_house_id"
    t.integer  "reviewer_id"
    t.integer  "barista_rate"
    t.integer  "savvy_rate"
    t.integer  "ambiance_rate"
    t.integer  "presentation_rate"
    t.integer  "aroma_rate"
    t.integer  "body_rate"
    t.integer  "brightness_rate"
    t.integer  "crema_rate"
    t.integer  "flavor_rate"
    t.float    "taster_correction"
    t.string   "consistency"
    t.string   "primary_taste"
    t.string   "size"
    t.float    "price"
    t.float    "cafe_r"
    t.float    "coffee_r"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "coffee_ratings", ["coffee_house_id"], :name => "index_coffee_ratings_on_coffee_house_id"
  add_index "coffee_ratings", ["reviewer_id"], :name => "index_coffee_ratings_on_reviewer_id"

  create_table "cup_manufacturers", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "website"
    t.text     "comments"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "machine_manufacturers", :force => true do |t|
    t.string   "name"
    t.string   "types"
    t.string   "groups"
    t.string   "models"
    t.string   "website"
    t.text     "comments"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "reviewers", :force => true do |t|
    t.string   "name"
    t.string   "nickname"
    t.string   "email"
    t.string   "reviewer_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "roasters", :force => true do |t|
    t.string   "name"
    t.string   "blends"
    t.string   "website"
    t.text     "comments"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
