Ext.define('CoffeeRatings.controller.Address',{
	extend: 'Ext.app.Controller',
	require: ['CoffeeRatings.store.Addresses','CoffeeRatings.model.Address','CoffeeRatings.view.ManagerAddress'],
	stores: ['Addresses'],
	models: ['Address'],
	views: ['ManagerAddress'],
	addressView: '',

	init: function(){
		addressView = this.getView('ManagerAddress').create();
		this.control({
			'manufacturerslist': {
				itemclick: this.clickOnGrip
			}
		});
	},

	clickOnGrip: function(grid, record){
		var addressModel = this.getModel('Address');
		var addressStore = this.getStore('Addresses');

		console.log(name);
		addressModel.proxy.setExtraParam('id',record.raw.coffee_house.id);
		addressStore.load({
			scope: this,
		    callback: function(records, operation, success) {
		        addressView.locationMap(
    				addressStore.getAt(0).get('street_line_1') + ',' + 
					addressStore.getAt(0).get('city') + ',' +
					addressStore.getAt(0).get('state') + ' ' +
					addressStore.getAt(0).get('zip_code'), 
					record.raw.coffee_house.name);
		    }
		});
	}
});