Ext.define('CoffeeRatings.controller.GeneralInfoChartController',{
	extend: 'Ext.app.Controller',
	views: ['GeneralInfoBeanChart','GeneralInfoBeanPanel','GeneralInfoCupChart','GeneralInfoCupPanel','GeneralInfoMachineChart','GeneralInfoMachinePanel','GeneralInfoMachineBasic','GeneralInfoBeanBasic','GeneralInfoCupBasic'],
	stores: ['GeneralInfoMachineStores','GeneralInfoBeanStores','GeneralInfoCupStores','GeneralInfoMachineBasicStores','GeneralInfoBeanBasicStores','GeneralInfoCupBasicStores'],
	models: ['GeneralInfoChartModel','GeneralInfoBasicModel'],
	//viewMachine:'',
	
	init: function(){
		
		//viewMachine = this.getView('GeneralInfoMachineBasic').create();
        //storeCoffee = this.getStore('GeneralInfoMachineBasicStores').create();

		this.control({
			'manufacturerslist': {
				itemclick: this.refreshViewMachineBasic
			}
		});

		this.control({
			'manufacturerslist': {
				itemclick: this.refreshViewBeanBasic
			}
		});

		this.control({
			'manufacturerslist': {
				itemclick: this.refreshViewCupBasic
			}
		});

		
		this.control({
			'manufacturerslist': {
				itemclick: this.refreshViewMachine
			}
		});

		this.control({
			'manufacturerslist': {
				itemclick: this.refreshViewBean
			}
		});

		this.control({
			'manufacturerslist': {
				itemclick: this.refreshViewCup
			}
		});


		
		this.callParent()
	},

	//===================================================================================

	refreshViewMachineBasic: function(grid,record){
		
		var storeCoffee = this.getStore('GeneralInfoMachineBasicStores');
		storeCoffee.proxy.setExtraParam('id',record.raw.coffee_house.id);
		storeCoffee.load(function(record,operation,success){
			console.log('datos cargados para basicMachine data ' + storeCoffee.getCount());
		});
			
	},

	refreshViewBeanBasic: function(grid,record){
		
		var storeCoffee = this.getStore('GeneralInfoBeanBasicStores');

		storeCoffee.proxy.setExtraParam('id',record.get('id'));
		
		storeCoffee.load(function(record,operation,success){
			console.log('datos cargados para basicBean ' + storeCoffee.getCount());
		});
	},


	refreshViewCupBasic: function(grid,record){
		var storeCoffee = this.getStore('GeneralInfoCupBasicStores');

		storeCoffee.proxy.setExtraParam('id',record.get('id'));
		//storeCoffee.proxy.setExtraParam('element',3);
		storeCoffee.load(function(record,operation,success){
			console.log('datos cargados para cupBean ' + storeCoffee.getCount());
		});
	},

	refreshViewMachine: function(grid,record){
		//console.log('asdfasdfasdf');
		
		var storeCoffee = this.getStore('GeneralInfoMachineStores');

		storeCoffee.proxy.setExtraParam('id',record.get('id'));
		//storeCoffee.proxy.setExtraParam('element',1);
		storeCoffee.load(function(record,operation,success){
			console.log('datos cargados para coffeRating ' + storeCoffee.getCount());
		});

	},

	refreshViewBean: function(grid,record){
		
		var storeCoffee = this.getStore('GeneralInfoBeanStores');
		storeCoffee.proxy.setExtraParam('id',record.get('id'));
		storeCoffee.load(function(record,operation,success){
			console.log('datos cargados para coffeRating ' + storeCoffee.getCount());
		});
	},


	refreshViewCup: function(grid,record){
		
		var storeCoffee = this.getStore('GeneralInfoCupStores');
		storeCoffee.proxy.setExtraParam('id',record.get('id'));
		storeCoffee.load(function(record,operation,success){
			console.log('datos cargados para coffeRating ' + storeCoffee.getCount());
		});
	}

	
	
});
