Ext.define('CoffeeRatings.controller.EspressoMachineManufacturer',{
	extend: 'Ext.app.Controller',
	views: ['ManufacturersList','CoffeeHouseRating','CoffeeRating'],
	stores: ['CoffeRatings','CoffeHouseRatings','EspressoMachineManufacturers'],
	models: ['CoffeRating','CoffeHouseRating','EspressoMachineManufacturer'],
	init: function(){
		var my = this;
		my.callParent();
	},
});
