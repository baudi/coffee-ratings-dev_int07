Ext.define('CoffeeRatings.controller.CoffeeRating',{
	extend: 'Ext.app.Controller',
	views: ['CoffeeRating'],
	stores: ['CoffeRatings','CoffeHouseRatings','AverageRatings'],

	init: function(){
		this.control({
			'manufacturerslist': {
				itemclick: this.coffeeRating
			}
		});
		this.control({
			'manufacturerslist': {
				itemclick: this.coffeeHouseRating
			}
		})
	},

	coffeeRating: function(grid, record){
		var coffeeStore = this.getStore('CoffeRatings');
        var average = this.calculateAverage(record,true);
        var averageRatingStore = this.getStore('AverageRatings');
        var recordRatingStore = averageRatingStore.getAt(0);

		coffeeStore.loadData([{
				"name":"aroma",
				"rating":record.get('aroma_rate'),
				"average":recordRatingStore.get('aroma')
				},{
				"name":"body",
				"rating":record.get('body_rate'),
				"average":recordRatingStore.get('body')
				},{
				"name":"brightness",
				"rating":record.get('brightness_rate'),
				"average":recordRatingStore.get('brightness')
				},{
				"name":"flavor",
				"rating":record.get('flavor_rate'),
				"average":recordRatingStore.get('flavor')
				},{
				"name":"crema",
				"rating":record.get('crema_rate'),
				"average":recordRatingStore.get('crema')
			}]);
	},

    coffeeHouseRating: function (grid, record){
    	var houseStore = this.getStore('CoffeHouseRatings');
    	var average = this.calculateAverage(record, false);
    	var averageRatingStore = this.getStore('AverageRatings');
        var recordRatingStore = averageRatingStore.getAt(0);

    	houseStore.loadData([
    			{"name":"barista",
				"rating":record.get('barista_rate'),
				"average":recordRatingStore.get('barista')
				},{
				"name":"savvy",
				"rating":record.get('savvy_rate'),
				"average":recordRatingStore.get('savvy')
				},{
				"name":"ambiance",
				"rating":record.get('ambiance_rate'),
				"average":recordRatingStore.get('ambiance')
				},{
				"name":"presentation",
				"rating":record.get('presentation_rate'),
				"average":recordRatingStore.get('presentation')
				}]);
    },

	calculateAverage: function(record, flag){
		if(flag){
		 return (record.get('aroma_rate')+record.get('body_rate') + record.get('brightness_rate') + record.get('crema_rate') + record.get('flavor_rate'))/5;
		}
		return (record.get('barista_rate') + record.get('savvy_rate') + record.get('ambiance_rate') + record.get('presentation_rate'))/4;

	}


})