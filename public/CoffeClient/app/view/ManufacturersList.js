Ext.define('CoffeeRatings.view.ManufacturersList', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.manufacturerslist',
    autoRender: true,
    autoShow: true,
    loadMask: true,
        selModel: {
            pruneRemoved: false
        },
        multiSelect: true,
        viewConfig: {
            trackOver: false
        },
        features:[{
            ftype: 'grouping',
            hideGroupedHeader: false
    }],

    store: 'EspressoMachineManufacturers',
    columns: [{
        text: 'Name',
        renderer: function(value, metaData, record) {
            return (record.raw.coffee_house.name);
        },
        flex: 4
    },{
        text: 'Type of Establishment',
        renderer: function(value, metaData, record) {
            return (record.raw.coffee_house.type_of_establishment);
        },
        flex: 4
    },{
        text: 'Average Coffee House',
        renderer: function(value, metaData, record) {
            return (record.get('barista_rate') + record.get('savvy_rate') + record.get('ambiance_rate') + record.get('presentation_rate'))/4;
        },
        flex: 4
    },{
        text: 'Average Coffee',
        renderer: function(value, metaData, record) {
            return (record.get('aroma_rate')+record.get('body_rate') + record.get('brightness_rate') + record.get('crema_rate') + record.get('flavor_rate'))/5;
        },
        flex: 4
    }

    ]
    /*
     * Initialize component
     */
    /*initComponent: function() {
        var me = this;
        me.callParent();
    }*/
});
