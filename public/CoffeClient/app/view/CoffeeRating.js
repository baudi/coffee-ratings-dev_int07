Ext.define('CoffeeRatings.view.CoffeeRating', {
    extend: 'Ext.chart.Chart',
    alias: 'widget.coffeerating',
    xtype: 'coffeerating',
    animate: true,
    insetPadding: 20,
    legend: {
        position: 'bottom'
    },
    store: 'CoffeRatings',
    
    axes: [{
        type: 'Radial',
        position: 'radial',
        fields: ['rating'],
        minimum: 0,
        maximum: 10,
        majorTickSteps: 1
    }],

    series: [{
        showInLegend: true,
        showMarkers: true,
        type: 'radar',
        xField: 'name',
        yField: 'rating',
        style: {
            opacity: 0.4
        },
        tips: {
            trackMouse: true,
            width: 100,
            height: 28,
            renderer: function(storeItem, item) {
                this.setTitle(storeItem.get('name') + ': ' + storeItem.get('rating'));
            }
        }
    }, {
        showInLegend: true,
        showMarkers: true,
        type: 'radar',
        xField: 'name',
        yField: 'average',
        style: {
            opacity: 0.4
        },
        tips: {
            trackMouse: true,
            width: 100,
            height: 28,
            renderer: function(storeItem, item) {
                this.setTitle(storeItem.get('name') + ': ' + storeItem.get('average'));
            }
        }
    }],

    /*
     * Initialize component
     */
    initComponent: function() {
        var me = this;
        me.callParent();
    }
});