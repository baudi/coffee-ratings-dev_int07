Ext.define('CoffeeRatings.view.ManagerAddress', {
	extend:'Ext.panel.Panel',
	alias : 'widget.addressMap',
	store: 'Addresses',
	autoShow: 'true',
	layout: 'border',
		width: 500,
		height: 300,
		bodyPadding : 5,
		autoShow: true,
		autoRender: true,
		items:[{
			region:'center',
			xtype:'container',
			id:'coffeemap'
		}],

		locationMap : function(nameAddress, nameCoffeHouse) {
			console.log(nameAddress);
			var geocoder = new google.maps.Geocoder();
			
			geocoder.geocode( {'address': nameAddress},function(results, status) 
			{
			    if (status == google.maps.GeocoderStatus.OK) 
			    {
			       	latitud = results[0].geometry.location.lat();
			       	logitud = results[0].geometry.location.lng();

			       	var mapOptions = {
			       		center: new google.maps.LatLng(latitud, logitud),
			       		zoom: 18,
			       		mapTypeId: google.maps.MapTypeId.ROADMAP
			        };

			        var map = new google.maps.Map(Ext.get('coffeemap').dom, mapOptions);

			        var marker = new google.maps.Marker({
			        	position: map.getCenter(),
					    map: map,
					    title: nameCoffeHouse
					});
					
                } else {
                    alert("Geocode was not successful for the following reason:" + status);
                }
    		});
		}
});