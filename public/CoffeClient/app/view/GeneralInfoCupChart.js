Ext.define('CoffeeRatings.view.GeneralInfoCupChart', {
    extend:'Ext.chart.Chart',
    alias:'widget.generalinfocupchart',
    store: 'GeneralInfoCupStores',
    insetPadding: 10,
    width: 170,
    height: 170,
    renderTo:Ext.getBody(),
    //animate: true,
    autoRender:true,
    autoShow:true,
    
    legend: {
        position: 'bottom'
    },
    axes: [{
        type: 'Radial',
        position: 'radial',
        fields: ['rating'],
        minimum: 0,
        maximum: 10,
        
        majorTickSteps: 10
    }],
    
    series: [{
        showInLegend: true,
        showMarkers: true,
        type: 'radar',
        xField: 'name',
        yField: 'rating',
        style: {
            opacity: 0.4
        },
        tips: {
            trackMouse: true,
            width: 100,
            height: 28,
            renderer: function(storeItem, item) {
                this.setTitle(storeItem.get('name') + ': ' + storeItem.get('rating'));
            }
        }
    },{
        showMarkers: true,
        showInLegend: true,
        type: 'radar',
        xField: 'name',
        yField: 'pm',
        
        style: {
             opacity: 0.4
        },
        tips: {
            trackMouse: true,
            width: 100,
            height: 28,
            renderer: function(storeItem, item) {
                this.setTitle(storeItem.get('name') + ': ' + storeItem.get('average'));
            }
        }
    }],
    initComponent: function() {
        var me = this;
        me.callParent();
        
    }    
});