Ext.define('CoffeeRatings.view.GeneralInfoMachineBasic',{
	extend: 'Ext.view.View',
	require:[
		'CoffeeRatings.store.GeneralInfoMachineBasicStores'
	],
	alias: 'widget.generalinfomachinebasic',
	store:'GeneralInfoMachineBasicStores',
		
	autoRender:true,
    autoShow:true,

    config:
    {
        tpl:'<tpl for=".">'+
		        '<div style="margin-bottom: 10px;" class="wrap">'+
		        '<br/><h7>Name :</h7><span>{mm_name}</span>'+
		        '<br/><h7>Country :</h7><span>{mm_country}</span>'+
		        '<br/><h7>Phone :</h7><span>{mm_phone}</span>'+
		        '<br/><h7>Total :</h7><span>{count_mm}</span><h7>&nbsp;coffee houses.</h7>'+
		        '<br/><h7>Web :</h7><br/><a href="{mm_url}">{mm_url}</a>'+
		        '</div>'+
		    '</tpl>',
    	itemSelector:'div.wrap'
	}


	
	
});