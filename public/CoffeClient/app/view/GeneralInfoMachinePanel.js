Ext.define('CoffeeRatings.view.GeneralInfoMachinePanel',{
	extend:'Ext.container.Container',
	alias:'widget.generalinfomachinepanel',
	border:true,
	autoRender:true,
	autoShow:true,
	maximizable : false,
	
	layout: {
        type: 'vbox',
        pack: 'start',
        align: 'stretch'
    },
	items : [{
		title : 'General Information',
		items:{
			xtype:'generalinfomachinebasic'
		}
		
	},{
		title : 'Used',
		items:{
			xtype:'generalinfomachinechart'
		}
	}]
});