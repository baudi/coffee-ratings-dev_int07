Ext.define('CoffeeRatings.view.GeneralInfoCupBasic',{
	extend: 'Ext.view.View',
	require:[
		'CoffeeRatings.store.GeneralInfoCupBasicStores'
	],
	alias: 'widget.generalinfocupbasic',
	store: 'GeneralInfoCupBasicStores',

	autoRender:true,
    autoShow:true,

    config:
    {
        tpl:'<tpl for=".">'+
		        '<div style="margin-bottom: 10px;" class="wrap">'+
		        '<br/><h7>Name : </h7><span>{cm_name}</span>'+
		        '<br/><h7>Country : </h7><span>{cm_country}</span>'+
		        '<br/><h7>Phone : </h7><span>{cm_phone}</span>'+
		        '<br/><h7>Total : </h7><span>{count_cm}</span><h7>&nbsp;coffee houses.</h7>'+
		        '<br/><h7>Web : </h7><br/><a href="{cm_url}">{cm_url}</a>'+
		        '</div>'+
		    '</tpl>',
    	itemSelector:'div.wrap'
	}
});