Ext.define('CoffeeRatings.view.GeneralInfoBeanPanel',{
	extend:'Ext.container.Container',
	alias:'widget.generalinfobeanpanel',
	border:true,
	autoRender:true,
	autoShow:true,
	maximizable : false,
	
	layout: {
        type: 'vbox',
        pack: 'start',
        align: 'stretch'
    },
	items : [{
		title : 'General Information',
		items:{
			xtype:'generalinfobeanbasic'
		}
	},{
		title : 'Used',
		items:{
			xtype:'generalinfobeanchart'
		}
	}]
});