Ext.define('CoffeeRatings.view.GeneralInfoCupPanel',{
	extend:'Ext.container.Container',
	alias:'widget.generalinfocuppanel',
	border:true,
	autoRender:true,
	autoShow:true,
	maximizable : false,
	
	layout: {
        type: 'vbox',
        pack: 'start',
        align: 'stretch'
    },
	items : [{
		title : 'General Information',
		items:{
			xtype:'generalinfocupbasic'
		}
		
	},{
		title : 'Used',
		items:{
			xtype:'generalinfocupchart'
		}
	}]
});