Ext.define('CoffeeRatings.view.CoffeeRatingsViewport', {
    extend: 'Ext.container.Viewport',
    xtype: 'coffeeratingsviewport',
    layout: {
        type: 'hbox',
        pack: 'start',
        align: 'stretch'
    },
    requires: [
        'CoffeeRatings.view.ManufacturersList',
        'CoffeeRatings.view.CoffeeHouseRating',
        'CoffeeRatings.view.CoffeeRating',
        'CoffeeRatings.view.GeneralInfoMachinePanel',
        'CoffeeRatings.view.GeneralInfoMachineChart',
        'CoffeeRatings.view.GeneralInfoMachineBasic',
        'CoffeeRatings.view.GeneralInfoBeanPanel',
        'CoffeeRatings.view.GeneralInfoBeanChart',
        'CoffeeRatings.view.GeneralInfoBeanBasic',
        'CoffeeRatings.view.GeneralInfoCupPanel',
        'CoffeeRatings.view.GeneralInfoCupChart',
        'CoffeeRatings.view.GeneralInfoCupBasic',
        'CoffeeRatings.view.ManagerAddress'
    ],
    items: [{
        flex: 1,
        xtype: 'container',
        border: false
    }, {
        width: 1024,
        height:2000,
        border: false,
        xtype: 'container',
        layout: {
            type: 'hbox',
            pack: 'start',
            align: 'stretch'
        },
        items: [{
            flex: 1,
            xtype: 'manufacturerslist',
            title: 'Manufacturers List' 
        }, {
            flex: 1,
            layout: {
                type: 'vbox',
                pack: 'start',
                align: 'stretch'
            },
            items: [{
                height: 250,
                border: false,
                type: 'container',
                layout: {
                    type: 'hbox',
                    pack: 'start',
                    align: 'stretch'
                },
                items: [{
                    flex: 1,
                    border: false,
                    title: 'Coffee Rating',
                    layout: 'fit',
                    items: {
                        xtype: 'coffeerating'
                    }
                }, {
                    flex: 1,
                    border: false,
                    title: 'Coffee House Raiting',
                    layout: 'fit',
                    items: {
                        xtype: 'coffeehouserating'
                    }
                }]
            }, {
                type: 'container',
                maximizable : true, 
                layout: {
                    type: 'hbox',
                    
                },
                items: [{
                    title: 'Expresso Machine Data',
                    items: {
                        xtype: 'generalinfomachinepanel'
                    }
                }, {
                    title: 'Coffee Beans Data',
                    items: {
                       xtype: 'generalinfobeanpanel'
                    }
                }, {
                    title: 'Cups Manufacturer Data',
                    items: {
                        xtype: 'generalinfocuppanel'
                    }
                }]
            }, {
                flex: 2,
                border: false,
                type: 'container',
                title: 'Coffee House Info',
                xtype: 'addressMap'
            }]
        }]
    }, {
        flex: 1,
        border: false,
        xtype: 'container'
    }],

    /*
     * Initialize component
     */
    initComponent: function() {
        var me = this;
        me.callParent();
    }
});
