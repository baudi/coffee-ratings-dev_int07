Ext.define('CoffeeRatings.view.GeneralInfoBeanBasic',{
	extend: 'Ext.view.View',
	require:[
		'CoffeeRatings.store.GeneralInfoBeanBasicStores'
	],
	alias: 'widget.generalinfobeanbasic',
	store:'GeneralInfoBeanBasicStores',

	autoRender:true,
    autoShow:true,

    config:
    {
        tpl:'<tpl for=".">'+
		        '<div style="margin-bottom: 10px;" class="wrap">'+
		        '<br/><h7>Name :</h7><span>{r_name}</span>'+
		        '<br/><h7>Country :</h7><span>{r_country}</span>'+
		        '<br/><h7>Phone :</h7><span>{r_phone}</span>'+
		        '<br/><h7>Total :</h7><span>{count_r}</span><h7>&nbsp;coffee houses.</h7>'+
		        '<br/><h7>Web :</h7><br/><a href="{url}">{url}</a>'+
		        '</div>'+
		    '</tpl>',
    	itemSelector:'div.wrap'
	}
});