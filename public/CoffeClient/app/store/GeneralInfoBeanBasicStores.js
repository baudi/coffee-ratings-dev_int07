Ext.define('CoffeeRatings.store.GeneralInfoBeanBasicStores',{
    extend: 'Ext.data.Store',
    model: 'CoffeeRatings.model.GeneralInfoBasicModel',  
    proxy: {
		type: 'rest',
		//url: 'app/data/dataBasic.json'
		url:'/coffee_houses/average',
		format:'json'
		
	}
	
});