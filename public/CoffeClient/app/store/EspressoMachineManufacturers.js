Ext.define('CoffeeRatings.store.EspressoMachineManufacturers', {
    extend: 'Ext.data.Store',
    requires: 'CoffeeRatings.model.EspressoMachineManufacturer',
    model: 'CoffeeRatings.model.EspressoMachineManufacturer',
    buffered: true,
    leadingBufferZone: 40,
    pageSize: 25,
    autoLoad: true,
    proxy: {
    	type: 'rest',
    	url: '/coffee_ratings/recover.json',
        reader: {
            type: 'json',
            root: 'ratings',
            totalProperty : 'count'
            },
	    simpleSortMode: true
    },
});
