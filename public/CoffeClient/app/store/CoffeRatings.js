Ext.define('CoffeeRatings.store.CoffeRatings',{
    extend: 'Ext.data.Store',
    model: 'CoffeeRatings.model.CoffeRating',  
});