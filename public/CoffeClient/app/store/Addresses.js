Ext.define('CoffeeRatings.store.Addresses',{
	extend: 'Ext.data.Store',
	model: 'CoffeeRatings.model.Address',
	autoLoad: 'false'
});