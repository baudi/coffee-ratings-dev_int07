Ext.define('CoffeeRatings.store.AverageRatings',{
	extend: 'Ext.data.Store',
	model: 'CoffeeRatings.model.AverageRating',
	autoLoad: 'true',
});