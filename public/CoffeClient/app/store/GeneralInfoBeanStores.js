Ext.define('CoffeeRatings.store.GeneralInfoBeanStores',{
    extend: 'Ext.data.Store',
    model: 'CoffeeRatings.model.GeneralInfoChartModel',  
    proxy: {
		type: 'rest',
		//url: 'app/data/data1.json'
		url:'/coffee_houses/average_customized.json',
		reader:{
			type:'json',
			root:'average_rating_per_roaster'
		}
	},
});