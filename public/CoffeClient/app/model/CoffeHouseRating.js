Ext.define('CoffeeRatings.model.CoffeHouseRating',{
	extend: 'Ext.data.Model',
	fields: ['average', 'name', 'rating']
});
