Ext.define('CoffeeRatings.model.EspressoMachineManufacturer', {
    extend: 'Ext.data.Model',
    fields: ['idEs',
    		 'barista_rate',
    		 'savvy_rate',
    		 'ambiance_rate',
    		 'presentation_rate',
    		 'aroma_rate',
    		 'body_rate',
    		 'brightness_rate',
    		 'crema_rate',
    		 'flavor_rate',
    		 'taster_correction'
    		],
    associations: [{ type: 'hasOne', model: 'CoffeeRatings.model.CoffeHouse' }]
});

Ext.define('CoffeeRatings.model.CoffeHouse',{
	extend: 'Ext.data.Model',
	fields:['comments',
			'created_at',
			'cup_manufacturer_id',
			'hours',
			'idCo',
			'machine_manufacturer_id',
			'name',
			'reviewer_id',
			'roaster_id',
			'type_of_establishment',
			'updated_at',
			'website' ]
} );
