Ext.define('CoffeeRatings.model.CoffeRating',{
	extend: 'Ext.data.Model',
	fields: ['average', 'name', 'rating']
});
