Ext.define('CoffeeRatings.model.GeneralInfoBasicModel',{
	extend: 'Ext.data.Model',
	fields: [
		'count_r',
		'r_name',
		'r_country',
		'r_phone',
		'url',

		'count_cm',
		'cm_name',
		'cm_country',
		'cm_phone',
		'cm_url',

		'count_mm',
		'mm_name',
		'mm_country',
		'mm_phone',
		'mm_url'
	]
});
