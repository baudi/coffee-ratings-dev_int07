Ext.define('CoffeeRatings.model.AverageRating',{
	extend: 'Ext.data.Model',
	fields: [
				'aroma',
				'body',
				'brightness',
				'barista',
				'savvy',
				'ambiance',
				'presentation',
				'crema',
				'flavor'
			],
	proxy: {
		type: 'rest',
		url: '/coffee_ratings/rate_averages',
		format: 'json'
	}
});