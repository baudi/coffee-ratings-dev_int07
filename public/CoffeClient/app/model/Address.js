Ext.define('CoffeeRatings.model.Address',{
    extend: 'Ext.data.Model',
	fields: [
		'addressable_id', 
		'addressable_type', 
		'city', 
		'country',
		'neighborhood', 
		'phone_number', 
		'state', 
		'street_line_1', 
		'street_line_2', 
		'zip_code'
	],
	 proxy: {
     	type: 'rest',
     	url: 'http://localhost:3000/addresses/find_by_coffee_house',
      	format: 'json'
    },
});